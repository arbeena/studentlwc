import { LightningElement, track, api } from 'lwc';
import getFieldSetMembers from '@salesforce/apex/StudentController.getFieldSetMembers';
import createStudent from '@salesforce/apex/StudentController.createStudent';
import {ShowToastEvent} from 'lightning/platformShowToastEvent';
import { NavigationMixin } from 'lightning/navigation';
import getRecord from '@salesforce/apex/StudentController.getRecord';

export default class Student extends NavigationMixin(LightningElement) {

    @track listField;
    @track objStu = {};
    @api recordId;
    @track dis =true;
    @track objStuColne = {};

    connectedCallback(){
        this.fieldHandler();
    }

    renderedCallback()
    {
        this.valueField();
    }

    createStudentRecord(){
        createStudent({objStu : this.objStu})
        .then(res => {
            if(this.recordId == null && res.Name == null){
                this.ShowToastEventHandler('ERROR', 'Enter the Required fields', 'error');
            }else if(this.recordId != null){
                this.ShowToastEventHandler('SUCCESS', `Student Record Updated successfully`, 'success');
                this.navigateHandler(this.recordId);
            }else{
                this.ShowToastEventHandler('SUCCESS', `${res.Name} Student Record created successfully`, 'success');
                this.navigateHandler(res.Id);

            }
            this.Id = res.Id;
            
        }).catch(error =>{
            console.log(error);
            
        })

    }

    fieldHandler(){
        getFieldSetMembers({objectApiName : 'Student__c',
                            fieldApiName : 'StudentFieldSet'})
        .then(res=>{
            this.listField = res;
            console.log(res);
            for(var i = 0; i < res.length; i++){
                if(res[i].type == "BOOLEAN"){
                    res[i].type = 'checkbox';
                }
            }
        }).catch(error=>{
            console.error(error);
        })
    }

    onChangeHandler(event){
        let fieldName = event.target.name;
        let value = event.target.value;
        if( this.recordId != null)
        {
            this.objStu['Id'] = this.recordId ; 
        }
        if(event.target.type == "checkbox"){
            this.objStu[fieldName] = event.target.checked;
        }else{
            this.objStu[fieldName] = value;
        }
    }


    ShowToastEventHandler(title, message, type){
        const evt = new ShowToastEvent({
            title : title,
            message : message,
            variant : type,
        });
        this.dispatchEvent(evt);
    }

    navigateHandler(recordId){
        this[NavigationMixin.Navigate]({
            type : "standard__recordPage",
            attributes: {
                recordId: recordId,
                objectApiName: 'Student__c',
                actionName: 'view'
            }
        })
    }

    cancelHandler(){
        console.log("CANCEL CALLED");
        console.log(this.recordId);
        if(this.recordId == null)
        {
            console.log(this.recordId);
            this.objStu={};
        this.template.querySelectorAll('lightning-input').forEach(element => {
          if(element.type === 'checkbox' || element.type === 'checkbox-button'){
            element.checked = false;
          }else{
            element.value = null;
          }      
        });
        }
        else
        {
            // this.navigateHandler(this.recordId);
            this.objStuColne = this.objStu;

        }
        
      }

    createStudentHandler(){
        this.createStudentRecord();
    }
    edit(){
        getRecord({recordId : this.recordId, objStu : this.objStu})
        .then(res => {
            this.ShowToastEventHandler('SUCCESS', `Student Record updated successfully`, 'success');

        })
        
    }

    valueField()
    {
        getRecord({recordId : this.recordId, objStu : this.objStu})
        .then(res => {
            
            for( var i =0; this.listField.length ; i++)
            {
                for (const [key, value] of Object.entries(res)) {

                    if(this.listField[i].apiName == key )
                    {
                        this.listField[i].val = value;
                    }
                    
                  }
            }
            
        }).catch( error => {
            console.error(error);
        })
    }

    resetval(event){
        console.log("reset");
        getRecord({recordId : this.recordId, objStu : this.objStu})
        .then(res => {
            
            for( var i =0; this.listField.length ; i++)
            {
                for (const [key, value] of Object.entries(res)) {

                    if(this.listField[i].apiName == key )
                    {
                        this.listField[i].val = event.target.value;
                    }
                    
                  }
            }
            
        }).catch( error => {
            console.error(error);
        })
    }
    buttonToggle()
    {
        this.dis = false;
    }
}