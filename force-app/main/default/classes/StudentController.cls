public with sharing class StudentController {
  
    @AuraEnabled
    public static List<FieldWrapper> getFieldSetMembers(string objectApiName, string fieldApiName){
        List<FieldWrapper> lstFieldWrapper =new List<FieldWrapper>();

        Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
        Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(objectApiName);
        Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
    
        //system.debug('====>' + DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName));
    
        Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldApiName);
        List<Schema.FieldSetMember> fieldSetMemberList =  fieldSetObj.getFields();
        for(Schema.FieldSetMember objFM : fieldSetMemberList){
            lstFieldWrapper.add(new FieldWrapper(objFM.getLabel(), objFM.getFieldPath(), objFM.getRequired(), string.valueOf(objFM.getType()), objFM.getDbRequired()));
        }
        //system.debug('fieldSetMemberList ====>' + fieldSetMemberList);  
        return lstFieldWrapper;
    }

    @AuraEnabled
    public static Object createStudent(Student__c objStu){
        try {
            // Student__c objStu = (Student__c)JSON.deserialize(strData, Student__c.class);
            if (String.isNotBlank(objStu.Id)) {
                update objStu;
                return objStu;
            } else
                if(String.isNotBlank(objStu.Name)){
                    insert objStu;
                }
                
                return objStu;
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public class FieldWrapper{
        @AuraEnabled public string label{get; set;}
        @AuraEnabled public string apiName{get; set;}
        @AuraEnabled public boolean isRequired{get; set;}
        @AuraEnabled public string type{get; set;}
        @AuraEnabled public boolean isDBRequired {get; set;}
        FieldWrapper(){}
        FieldWrapper(string label, string apiName, boolean isRequired, string type, boolean isDBRequired){
            this.label = label;
            this.apiName = apiName;
            this.isRequired = isRequired;
            this.type = type;
            this.isDBRequired = isDBRequired;
        }
    }

    @AuraEnabled
    public static Student__c getRecord(String recordId, Student__c objStu){
        try {
            //Student__c objStudentEditRecord = [SELECT Id,Name, Department__c, Is_Active__c, Age__c, Birthdate__c, Admission_Date__c, Email__c,SocialProfile__c, Highest_Degree_Percentage__c, Description__c  FROM Student__c WHERE Id = :recordId];
           // objStudentEditRecord.Name = objStu.Name;
            //update objStudentEditRecord;
            // return objStudentEditRecord;

            String studentQuery = 'SELECT ';
            for(FieldWrapper fW : getFieldSetMembers('Student__c', 'StudentFieldSet')){
                studentQuery += fW.apiName + ', ';      
            }
            String studentId = '\''+recordId+'\'';
            studentQuery += ' Id FROM Student__c WHERE Id = '+studentId+'';
            Student__c objStudent = Database.query(studentQuery);
            return objStudent;        
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
  
}
